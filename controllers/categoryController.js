'use strict';

const _ = require('lodash');
const log4js = require('log4js');
const logger = log4js.getLogger('categoryController');
const CategoryManager = require('../services/CategoryManager');


module.exports = {
    find: async (req, res)=>{
        logger.trace('find');

        const CategoryTree = CategoryManager.getTree();
        // for testing
        // const CategoryTree = CategoryManager.getMockData();

        if(!CategoryTree){
            logger.info('failed to fetch data')
            return res.status(401).send({
                success: false,
                data: {}
            });
        }

        return res.status(200).send({
            success: true,
            data: CategoryTree
        });
    },
    create: async (req, res)=>{
        logger.trace('create');

        const dbAttrs = _.pick(req.body, [
            'name',
            'parentId',
            'type'
        ]);
        const newItemId = CategoryManager.add(dbAttrs);

        if(!newItemId){
            logger.info('failed to create new Item')
            return res.status(401).send({
                success: false,
                data: {}
            });
        }

        res.status(200).send({
            success: true,
            data: {id: newItemId}
        });
    },
    update: async (req, res)=>{
        logger.trace('update');

        const dbAttrs = _.pick(req.body, [
            'name',
            'id',
        ]);

        const updatedItemId = CategoryManager.rename(dbAttrs);

        if(!updatedItemId){
            logger.info(`failed to rename Item ${dbAttrs.id}`)
            return res.status(401).send({
                success: false,
                data: {}
            });
        }

        res.status(200).send({
            success: true,
            data: {id: updatedItemId}
        });
    },
    delete: async (req, res)=>{
        logger.trace('delete');

        const dbAttrs = _.pick(req.body, [
            'id',
            'parent'
        ]);

        const updatedItem = CategoryManager.delete(dbAttrs);

        if(!updatedItem){
            logger.info(`failed to delete Item ${dbAttrs.id}`)
            return res.status(401).send({
                success: false,
                data: {}
            });
        }

        res.status(200).send({
            success: true,
            data: {}
        });
    },

}
