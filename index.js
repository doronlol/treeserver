'use strict';

const config = require('config');
const log4js = require('log4js');
log4js.configure(config.loggerConfig);
const logger = log4js.getLogger('index');
const bodyParser = require('body-parser');
const CategoryRoutes = require('./routes/CategoryRoutes');
const cors = require('cors');

const express = require('express')
const app = express()
const port = 1000

async function init(){
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(cors());

    app.use('/category/', CategoryRoutes);


    app.listen(port, () => console.log(`Listening on port ${port}!`))
}

init().catch(err => {
    logger.info(err);
    process.exit(1);
});
