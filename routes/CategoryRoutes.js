const express = require('express');
const router = express.Router();
const categoryController = require('../controllers/categoryController');

router.get('/', categoryController.find);
router.post('/', categoryController.create);
router.put('/', categoryController.update);
router.delete('/', categoryController.delete);


module.exports = router;
