'use strict';

const _ = require('lodash');
const log4js = require('log4js');
const logger = log4js.getLogger('CategoryManager');
const { v4: uuidv4 } = require('uuid');

class CategoryManager {
    constructor() {
        _.bindAll(this, Object.getOwnPropertyNames(this.constructor.prototype));
        this.root = {
            type: "parent",
            name: "",
            children: [],
            id: "0",
            parentId: ""
        }
    }

    getTree(){
        return JSON.stringify(this.root.children);
    }

    add(data){
        logger.trace('Add Item');
        const newId = uuidv4();
        if(data.parentId === "0"){
            this.root.children = this._appendNewChild( this.root,{
                name: "new Category",
                id: newId,
                parentId: "0",

            });

            return newId
        }

        const parent = this._find(data.parentId)
        if(!parent) return null;

        parent.children = this._appendNewChild(parent, {
            name: data.name,
            id: newId,
            parentId: data.parentId,
            type:data.type
        });

        return newId;
    }

    rename(data){
        logger.trace('Rename Item');
        const current = this._find(data.id)
        if(!current) return null;

        current.name = data.name

        return current.id;
    }

    delete(data){
        logger.trace('Delete Item');

        let parent = this._find(data.parent)
        if(!parent) {
            parent = this.root
        }

        const index = parent.children.findIndex(item=> item.id === data.id);

        if(index < 0){
            logger.info(`item ${data.id} was not found in tree`)
            return null;
        }

        let newChildArr = [...parent.children]
        newChildArr.splice(index, 1)

        parent.children = newChildArr;

        return true;
    }

    _appendNewChild(parent, data){
        const newCategory = {
            name: data.name,
            children: [],
            id: data.id,
            type: data.type? data.type : "child",
            parentId: data.parentId,

        }

        return  [...parent.children, ...[newCategory]];
    }

    _find(target){
        const queue = [this.root];

        while(queue.length){
            const current = queue.shift();
            if(current.id === target){
                return current;
            }
            if(current.children.length){
                current.children.forEach((child)=>queue.push(child))
            }

        }

        return false;
    }

    getMockData(){
        return  [
            {
                type: "parent",
                name: "name 1",
                id:1,
                parentId: 0,
                children: [],
            },
            {
                type: "parent",
                name: "name 2",
                id:2,
                parentId: 0,
                children: [
                    {
                        type: "child",
                        name: "name 2 - 1",
                        id:3,
                        parentId: 2,
                        children: [],
                    },
                    {
                        type: "child",
                        name: "name 2 - 2",
                        id:4,
                        parentId: 2,
                        children: [],
                    },
                    {
                        type: "child",
                        name: "name 2 - 3",
                        id:5,
                        parentId: 2,
                        children: [],
                    },
                ],

            },
            {
                type: "parent",
                name: "name 3",
                id:6,
                parentId: 0,
                children: [
                    {
                        type: "child",
                        name: "name 3 - 1",
                        id:7,
                        parentId: 6,
                        children: [],
                    },
                    {
                        type: "child",
                        name: "name 3 - 2",
                        id:8,
                        parentId: 6,
                        children: [],
                    },
                    {
                        type: "parent",
                        name: "name 3 - 3",
                        id:9,
                        parentId: 6,
                        children: [
                            {
                                type: "child",
                                name: "last 1",
                                id:10,
                                parentId: 9,
                                children: [],
                            },
                            {
                                type: "child",
                                name: "last 1",
                                id:11,
                                parentId: 9,
                                children: [],
                            },
                            {
                                type: "child",
                                name: "last 1",
                                id:12,
                                parentId: 9,
                                children: [],
                            },
                        ],
                    },
                ],
            },


        ]

    }

}
module.exports = new CategoryManager();
